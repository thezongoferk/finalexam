using Microsoft.AspNetCore.Identity;
using System; 
using System.ComponentModel.DataAnnotations; 


namespace FeverCalender.Models 
{ 
    public class Calender 
    { 
        public int CalenderID { get; set;} 
    }
   
    public class table 
    { 
        public int tableID { get; set; }
        
        public int CalenderID { get; set; } 
        public table calId { get; set; } 
        
        public string Date { get; set; } 
        public string Status { get; set; }

        public string UserId {get; set;} 
        public CalUser postUser {get; set;}
     } 
    
     public class CalUser : IdentityUser{ 
        public string FirstName { get; set;}
        public string LastName { get; set;}
    }
}