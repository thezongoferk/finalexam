using FeverCalender.Models; 
using Microsoft.EntityFrameworkCore; 
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace FeverCalender.Data 
{ 
    public class CalenderContext : IdentityDbContext<CalUser>
    { 
        public DbSet<table> TableList { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) 
        { 
            optionsBuilder.UseSqlite(@"Data source=FeverCalender.db");
        }
    }
 }