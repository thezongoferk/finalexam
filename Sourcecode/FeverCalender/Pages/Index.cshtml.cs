﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

using Microsoft.EntityFrameworkCore; 
using FeverCalender.Data;
using FeverCalender.Models;

namespace FeverCalender.Pages
{
   public class IndexModel : PageModel 
   { 
       private readonly FeverCalender.Data.CalenderContext _context; 
       public IndexModel(FeverCalender.Data.CalenderContext context) 
       { 
           _context = context; 
        } 
        
        public IList<table> cal { get;set; }
        
        public async Task OnGetAsync() 
        { 
            cal = await _context.TableList .Include(n => n.calId).ToListAsync(); 
        }
     } 
}
