using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using FeverCalender.Data;
using FeverCalender.Models;

namespace FeverCalender.Pages.CalenderAdmin
{
    public class EditModel : PageModel
    {
        private readonly FeverCalender.Data.CalenderContext _context;

        public EditModel(FeverCalender.Data.CalenderContext context)
        {
            _context = context;
        }

        [BindProperty]
        public table table { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            table = await _context.TableList.FirstOrDefaultAsync(m => m.tableID == id);

            if (table == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(table).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!tableExists(table.tableID))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool tableExists(int id)
        {
            return _context.TableList.Any(e => e.tableID == id);
        }
    }
}
