using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using FeverCalender.Data;
using FeverCalender.Models;

namespace FeverCalender.Pages.CalenderAdmin
{
    public class DetailsModel : PageModel
    {
        private readonly FeverCalender.Data.CalenderContext _context;

        public DetailsModel(FeverCalender.Data.CalenderContext context)
        {
            _context = context;
        }

        public table table { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            table = await _context.TableList.FirstOrDefaultAsync(m => m.tableID == id);

            if (table == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
