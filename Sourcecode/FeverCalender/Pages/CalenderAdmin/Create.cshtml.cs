using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using FeverCalender.Data;
using FeverCalender.Models;

namespace FeverCalender.Pages.CalenderAdmin
{
    public class CreateModel : PageModel
    {
        private readonly FeverCalender.Data.CalenderContext _context;

        public CreateModel(FeverCalender.Data.CalenderContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty]
        public table table { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.TableList.Add(table);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}