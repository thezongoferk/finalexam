using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using NewsReport.Data;

[assembly: HostingStartup(typeof(FeverCalender.Areas.Identity.IdentityHostingStartup))]
namespace FeverCalender.Areas.Identity
{
    public class IdentityHostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((context, services) => {
                services.AddDbContext<NewsReportContext>(options =>
                    options.UseSqlServer(
                        context.Configuration.GetConnectionString("NewsReportContextConnection")));

                services.AddDefaultIdentity<IdentityUser>()
                    .AddEntityFrameworkStores<NewsReportContext>();
            });
        }
    }
}